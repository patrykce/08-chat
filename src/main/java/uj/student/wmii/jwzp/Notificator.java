package uj.student.wmii.jwzp;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uj.student.wmii.jwzp.model.Message;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Notificator implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Notificator.class);
    private final String userName;
    private final String exchangeName;
    private final String exchangeType;
    private final String queue;
    private final ConnectionFactory connectionFactory;


    public Notificator(String userName, String exchangeName, String exchangeType, String queue, ConnectionFactory connectionFactory) {
        this.queue = queue;
        this.userName = userName;
        this.exchangeName = exchangeName;
        this.exchangeType = exchangeType;
        this.connectionFactory = connectionFactory;
    }


    private void notifyClients() throws IOException, TimeoutException {
        Scanner in = new Scanner(System.in);
        try (Connection connection = connectionFactory.newConnection(); Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(exchangeName, exchangeType);
            channel.queueDeclare(queue, false, false, false, null);
            while (true) {
                String content = in.nextLine();
                if (content.equalsIgnoreCase("stop")) {
                    channel.basicPublish(exchangeName, queue, null, getExitMsg().getBytes(StandardCharsets.UTF_8));
                    System.exit(0);
                }
                Message msg = new Message(userName, content);
                channel.basicPublish(exchangeName, queue, null, msg.parseAsStringMessage().getBytes(StandardCharsets.UTF_8));
                logger.info("Sent: {}", msg);
            }
        }
    }

    private String getExitMsg() {
        return String.format("User: %s exited chat", userName);
    }

    @Override
    public void run() {
        try {
            notifyClients();
        } catch (IOException | TimeoutException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}