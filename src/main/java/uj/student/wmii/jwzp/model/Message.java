package uj.student.wmii.jwzp.model;

public class Message {
    private final String user;
    private final String message;

    public Message(String user, String message) {
        this.user = user;
        this.message = message;
    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "user='" + user + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public String parseAsStringMessage() {
        return "{\"user\":\" " + user + " \",\"message\":\"" + message + "\"}";
    }

}
