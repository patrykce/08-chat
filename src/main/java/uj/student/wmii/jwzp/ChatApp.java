package uj.student.wmii.jwzp;

import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class ChatApp {
    private static final String PROPERTIES_PATH = "src/main/resources/application.properties";
    private static final Logger logger = LoggerFactory.getLogger(ChatApp.class);

    public static void main(String[] args) throws IOException, TimeoutException {
        String userName = requestUserName();
        Properties properties = getProperties();
        if (properties == null) return;
        System.out.println("\nHi! " + userName + ". Now you can sent messages!");
        ConnectionFactory receiverConnectionFactory = getConnectionFactory(properties);
        ConnectionFactory notificatorConnectionFactory = getConnectionFactory(properties);
        Receiver receiver = getReceiver(properties, receiverConnectionFactory);
        Notificator notificator = getNotificator(properties, userName, notificatorConnectionFactory);
        receiver.notificationsHandle();
        Thread client = new Thread(notificator);
        client.start();
    }

    private static Receiver getReceiver(Properties properties, ConnectionFactory receiverConnectionFactory) {
        return new Receiver(receiverConnectionFactory, properties.getProperty("exchangeName"), properties.getProperty("exchangeType"), properties.getProperty("queue"));
    }

    private static String requestUserName() {
        System.out.println("Type user name: ");
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    private static Properties getProperties() {
        try {
            Properties properties = new Properties();
            FileReader reader = new FileReader(PROPERTIES_PATH);
            properties.load(reader);
            return properties;
        } catch (IOException e) {
            logger.error("cannot bind properties file with properties object. Stop app");
            return null;
        }
    }

    private static ConnectionFactory getConnectionFactory(Properties properties) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(properties.getProperty("host"));
        factory.setUsername(properties.getProperty("user"));
        factory.setVirtualHost(properties.getProperty("virtualHost"));
        factory.setPassword(properties.getProperty("password"));
        return factory;
    }

    private static Notificator getNotificator(Properties properties, String userName, ConnectionFactory connectionFactory) {
        String exchangeName = properties.getProperty("exchangeName");
        String exchangeType = properties.getProperty("exchangeType");
        String queue = properties.getProperty("queue");
        return new Notificator(userName, exchangeName, exchangeType, queue, connectionFactory);
    }

}
