package uj.student.wmii.jwzp;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class Receiver {
    private final ConnectionFactory connectionFactory;
    private final String exchange;
    private final String exchangeType;
    private final String queue;


    public Receiver(ConnectionFactory connectionFactory, String exchange, String exchangeType, String queue) {
        this.connectionFactory = connectionFactory;
        this.exchange = exchange;
        this.exchangeType = exchangeType;
        this.queue = queue;
    }

    public void notificationsHandle() throws IOException, TimeoutException {
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(exchange, exchangeType);
        channel.queueDeclare(queue, false, false, false, null);
        channel.queueBind(queue, exchange, queue);
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            displayNotification(message);

        };
        boolean autoAck = false;
        channel.basicConsume(queue, autoAck, deliverCallback, consumerTag -> {
        });
    }

    private void displayNotification(String content) {
        System.out.println(content);
    }

}
